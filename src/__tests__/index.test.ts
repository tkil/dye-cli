const Dye = require("../index");

describe("Commands", () => {
  describe("darker", () => {
    it("should print a 10% darker hex", () => {
      // Arrange
      const consoleInfoRestore = console.info;
      console.info = jest.fn();
      // Act
      Dye.command({ args: { hexCode: "7F7F7F" }, flags: { darken: "10" } });
      // Assert
      expect(console.info).toBeCalledWith("#727272");
      // Restore
      console.info = consoleInfoRestore;
    });
  });
  describe("lighten", () => {
    it("should print a 10% lighter hex", () => {
      // Arrange
      const consoleInfoRestore = console.info;
      console.info = jest.fn();
      // Act
      Dye.command({ args: { hexCode: "7F7F7F" }, flags: { lighten: "10" } });
      // Assert
      expect(console.info).toBeCalledWith("#8C8C8C");
      // Restore
      console.info = consoleInfoRestore;
    });
  });
});
