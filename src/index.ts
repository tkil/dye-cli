import { Command, flags } from "@oclif/command";
import { darken, lighten } from "./utils/coloring";

class DyeCli extends Command {
  static description = "describe the command here";

  static flags = {
    version: flags.version({ char: "v" }),
    help: flags.help({ char: "h" }),
    darken: flags.string({ char: "d", description: "lighten by percentage" }),
    lighten: flags.string({ char: "l", description: "lighten by percentage" }),
  };

  static args = [{ name: "hexCode" }];

  async run() {
    await DyeCli.command(this.parse(DyeCli));
  }

  static async command({ args, flags }: any) {
    switch (true) {
      case Boolean(flags.darken):
        DyeCli.darken(args.hexCode, flags.darken);
      case Boolean(flags.lighten):
        DyeCli.lighten(args.hexCode, flags.lighten);
    }
  }

  static darken(hexCode: string, percentage: number) {
    console.info(darken(hexCode, percentage));
  }

  static lighten(hexCode: string, percentage: number) {
    console.info(lighten(hexCode, percentage));
  }
}

export = DyeCli;
