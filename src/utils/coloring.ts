import * as Color from "color";

export const darken = (hexCode: string, percentage: number) => {
  return Color(`#${hexCode}`)
    .darken(percentage / 100)
    .hex();
};

export const lighten = (hexCode: string, percentage: number) => {
  return Color(`#${hexCode}`)
    .lighten(percentage / 100)
    .hex();
};
