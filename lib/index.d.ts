import { Command, flags } from "@oclif/command";
declare class DyeCli extends Command {
    static description: string;
    static flags: {
        version: import("@oclif/parser/lib/flags").IBooleanFlag<void>;
        help: import("@oclif/parser/lib/flags").IBooleanFlag<void>;
        lighten: flags.IOptionFlag<string | undefined>;
    };
    static args: {
        name: string;
    }[];
    run(): Promise<void>;
    static command({ args, flags }: any): Promise<void>;
    static lighten(hexCode: string, percentage: number): void;
}
export = DyeCli;
