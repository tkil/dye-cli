"use strict";
const command_1 = require("@oclif/command");
const coloring_1 = require("./utils/coloring");
class DyeCli extends command_1.Command {
    async run() {
        await DyeCli.command(this.parse(DyeCli));
    }
    static async command({ args, flags }) {
        switch (true) {
            case Boolean(flags.lighten):
                coloring_1.lighten(args.hexCode, flags.lighten);
        }
    }
    static lighten(hexCode, percentage) {
        console.log("here");
        console.info(coloring_1.lighten(hexCode, percentage));
    }
}
DyeCli.description = "describe the command here";
DyeCli.flags = {
    // add --version flag to show CLI version
    version: command_1.flags.version({ char: "v" }),
    help: command_1.flags.help({ char: "h" }),
    lighten: command_1.flags.string({ char: "l", description: "lighten by percentage" }),
};
DyeCli.args = [{ name: "hexCode" }];
module.exports = DyeCli;
