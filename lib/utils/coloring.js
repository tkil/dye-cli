"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lighten = exports.darken = void 0;
const Color = require("color");
exports.darken = (hexCode, percentage) => {
    return Color(`#${hexCode}`)
        .darken(percentage / 100)
        .hex();
};
exports.lighten = (hexCode, percentage) => {
    return Color(`#${hexCode}`)
        .lighten(percentage / 100)
        .hex();
};
