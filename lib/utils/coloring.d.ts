export declare const darken: (hexCode: string, percentage: number) => string;
export declare const lighten: (hexCode: string, percentage: number) => string;
