"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const coloring_1 = require("../coloring");
describe("Coloring util", () => {
    describe("darken()", () => {
        it("should darken hex by 10%", () => {
            // Act
            const actual = coloring_1.darken("7F7F7F", 10);
            // Assert
            expect(actual).toEqual("#727272");
        });
    });
    describe("lighten()", () => {
        it("should lighten hex by 10%", () => {
            // Act
            const actual = coloring_1.lighten("7F7F7F", 10);
            // Assert
            expect(actual).toEqual("#8C8C8C");
        });
    });
});
