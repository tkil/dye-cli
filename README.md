dye-cli
=======

CLI for generating colors and palettes

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/dye-cli.svg)](https://npmjs.org/package/dye-cli)
[![Downloads/week](https://img.shields.io/npm/dw/dye-cli.svg)](https://npmjs.org/package/dye-cli)
[![License](https://img.shields.io/npm/l/dye-cli.svg)](https://github.com/tylerkilburn/dye-cli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g dye-cli
$ dye COMMAND
running command...
$ dye (-v|--version|version)
dye-cli/0.0.0 darwin-x64 node-v14.15.0
$ dye --help [COMMAND]
USAGE
  $ dye COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->

<!-- commandsstop -->
